# Spring Web Utilities

## Usage

Add this library as a dependency to your project's pom.xml.

```xml
<dependency>
  <groupId>edu.byu.hbll</groupId>
  <artifactId>spring-web</artifactId>
  <version>1.1.0</version>
</dependency>
```

### OAuth2HttpInterceptor

The `OAuth2HttpInterceptor` allows you to seamlessly access resources that have been protected with
OAuth2 using the client credentials flow. In other words, it facilitates one api accessing another
using OAuth2.

If your application only needs to access one OAuth2 server with one set of credentials, you can use
spring boot autoconfiguration:

```yaml
byuhbll:
  spring-web:
    oauth2:
      token-uri: https://example.com/token
      client-id: myClientId
      client-secret: myClientSecret
      login-attempts-allowed: 1 # optional. default: 3
```

```java
@Autowired OAuth2HttpInterceptor interceptor;
```

If you need to construct multiple `OAuth2HttpInterceptor`s or you're not using spring boot, the
following is an example of creating a new instance:

```java
OAuth2HttpInterceptor interceptor = OAuth2HttpInterceptor.builder()
    .tokenUri(URI.create("https://example.com/token"))
    .clientId("myClientId")
    .clientSecret("myClientSecret")
    .loginAttemptsAllowed(1)
    .build();
```

### OAuth2RestTemplate

The `OAuth2RestTemplate` is a `RestTemplate` implementation that requires an
`OAuth2HttpInterceptor`.

If your application uses Spring Boot Autoconfiguration, simply provide an `OAuth2HttpInterceptor`
as a bean either by using the above autoconfiguration method or by registering your own. If an
`OAuth2HttpInterceptor` bean is present, an `OAuth2RestTemplate` will automatically be configured
with that bean and will be available for injection.

```java
@Autowired OAuth2RestTemplate restTemplate;
```

If you need more than one, it can be constructed as follows:

```java
OAuth2HttpInterceptor interceptor = ...
OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(interceptor);
```

## Developing This Project

Note: These instructions assume Docker and VS Code, with the Dev Containers extension, are installed.

Git clone the project locally and open it with VS Code. When prompted, reopen the project in a dev container. It will take a couple of minutes to download all of the dependencies and prepare the container for Java development. Proceed with development once the initialization is complete.

Tests can be run from within VS Code. Click the Testing icon and click "Run Tests".

To build and install the project locally in order to include it in another project for testing, run the following from the project directory on the host machine.

```
docker run -it -u $UID:$(id -g) -e MAVEN_CONFIG=/var/maven/.m2 -v ~/.m2:/var/maven/.m2 -v .:/project maven mvn -f /project/pom.xml -Duser.home=/var/maven clean package install
```

Building and deploying this project is handled by the GitLab pipeline. There are two possible destinations for the built project: the BYU HBLL internal Maven repository or the central Maven repository. Use the internal repository for non-public releases and the central repository for public open source releases. To instruct the pipeline to deploy to Maven Central, set the `MAVEN_CENTRAL_DEPLOY` CI/CD variable to `true`. Deploying to either the internal or central repository requires manually playing the deploy job. Note: Deployments to Maven Central are permanent.

## License

[License](LICENSE.md)