package edu.byu.hbll.spring.web.starter;

import static org.assertj.core.api.Assertions.assertThat;

import edu.byu.hbll.spring.web.oauth2.OAuth2HttpInterceptor;
import java.net.URI;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.AutoConfigurations;
import org.springframework.boot.test.context.runner.ApplicationContextRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

public class OAuth2HttpInterceptorAutoConfigurationTest {

  private final ApplicationContextRunner contextRunner =
      new ApplicationContextRunner()
          .withPropertyValues(
              "byuhbll.spring-web.oauth2.client-id=testid",
              "byuhbll.spring-web.oauth2.client-secret=supersecretclientsecret",
              "byuhbll.spring-web.oauth2.token-uri=http://example.com/",
              "byuhbll.spring-web.oauth2.login-attempts-allowed=2")
          .withConfiguration(AutoConfigurations.of(OAuth2HttpInterceptorAutoConfiguration.class));

  @Test
  public void clientExists() {
    this.contextRunner.run(
        context -> assertThat(context).hasSingleBean(OAuth2HttpInterceptor.class));
  }

  @Test
  public void shouldPreferUserBean() {
    this.contextRunner
        .withUserConfiguration(OAuth2HttpInterceptorTestUserConfig.class)
        .run(
            context ->
                assertThat(context.getBean(OAuth2HttpInterceptor.class).getLoginAttemptsAllowed())
                    .isEqualTo(7));
  }

  @Test
  public void shouldNotCrashWhenNotUsingAutoConfiguration() {
    new ApplicationContextRunner()
        .withConfiguration(AutoConfigurations.of(OAuth2HttpInterceptorAutoConfiguration.class))
        .run(context -> assertThat(context).doesNotHaveBean(OAuth2HttpInterceptor.class));
  }

  @Configuration
  static class OAuth2HttpInterceptorTestUserConfig {

    @Bean
    public OAuth2HttpInterceptor oAuth2HttpInterceptor() {
      return OAuth2HttpInterceptor.builder()
          .clientId("client")
          .clientSecret("secret")
          .tokenUri(URI.create("http://example.com/token"))
          .loginAttemptsAllowed(7)
          .build();
    }
  }
}
