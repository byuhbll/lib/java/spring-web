package edu.byu.hbll.spring.web.oauth2;

import io.specto.hoverfly.junit.core.Hoverfly;
import io.specto.hoverfly.junit.core.SimulationSource;
import io.specto.hoverfly.junit.dsl.HoverflyDsl;
import io.specto.hoverfly.junit.dsl.ResponseCreators;
import io.specto.hoverfly.junit5.HoverflyExtension;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

/** OAuth2HttpInterceptorIT */
@ExtendWith(HoverflyExtension.class)
public class OAuth2HttpInterceptorIT {

  @Test
  public void testApiWithOAuth(Hoverfly hoverfly) throws IOException {

    hoverfly.simulate(
        SimulationSource.dsl(
            HoverflyDsl.service("http://example.com")
                .post("/token")
                .body("grant_type=client_credentials")
                .willReturn(
                    ResponseCreators.success(
                        Files.readString(Paths.get("src/test/resources/response-token.json")),
                        MediaType.APPLICATION_JSON_VALUE))
                .get("/api")
                .header("Authorization", "Bearer faketoken")
                .willReturn(ResponseCreators.success("hello", MediaType.TEXT_PLAIN_VALUE))));

    OAuth2HttpInterceptor interceptor =
        OAuth2HttpInterceptor.builder()
            .tokenUri(URI.create("http://example.com/token"))
            .clientId("myClientIt")
            .clientSecret("myClientSecret")
            .build();

    RestTemplate client = new OAuth2RestTemplate(interceptor);
    client.getForEntity("http://example.com/api", String.class);
  }
}
