package edu.byu.hbll.spring.web.oauth2;

import java.util.List;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * RestTemplate for working with services secured with OAuth2.
 *
 * <p>This RestTemplate accepts an {@link OAuth2HttpInterceptor} at construction time. The
 * interceptor is used to interact with an OAuth2 server using the {@code client_credentials} grant
 * and the resource server using the resulting bearer token.
 */
public class OAuth2RestTemplate extends RestTemplate {

  /**
   * Construct an OAuth2RestTemplate with an OAuth2HttpInterceptor.
   *
   * @param interceptor the interceptor to use.
   */
  public OAuth2RestTemplate(OAuth2HttpInterceptor interceptor) {
    this.getInterceptors().add(interceptor);
  }

  public OAuth2RestTemplate(
      OAuth2HttpInterceptor interceptor, ClientHttpRequestFactory requestFactory) {
    super(requestFactory);
    this.getInterceptors().add(interceptor);
  }

  public OAuth2RestTemplate(
      OAuth2HttpInterceptor interceptor, List<HttpMessageConverter<?>> messageConverters) {
    super(messageConverters);
    this.getInterceptors().add(interceptor);
  }
}
