package edu.byu.hbll.spring.web.oauth2;

import com.fasterxml.jackson.databind.JsonNode;
import java.io.IOException;
import java.net.URI;
import java.time.Instant;
import java.util.Arrays;
import lombok.Builder;
import lombok.Getter;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.RestTemplate;

/**
 * ClientHttpRequestInterceptor implementation for OAuth2.
 *
 * <p>This class implements the {@code client_credentials} flow in OAuth2, adding the functionality
 * to Spring's {@link RestTemplate}.
 */
public class OAuth2HttpInterceptor implements ClientHttpRequestInterceptor {

  private static final String CLIENT_CREDENTIALS_GRANT = "grant_type=client_credentials";
  private static final int DEFAULT_LOGIN_ATTEMPTS_ALLOWED = 3;

  private URI tokenUri;
  private HttpEntity<String> loginEntity;
  private RestTemplate client = new RestTemplate(); // Intentionally not injected

  private Instant expiration = Instant.EPOCH;
  private String token;

  @Getter private int loginAttemptsAllowed;

  /**
   * Constructs a new instance.
   *
   * <p>This constructor is private and is for use by the Lombok-generated builder.
   *
   * @param clientId the oauth2 client ID
   * @param clientSecret the oauth2 client secret
   * @param tokenUri the oauth2 access_token URI
   * @param loginAttemptsAllowed the number of times to attempt to obtain a token before giving up
   */
  @Builder
  private OAuth2HttpInterceptor(
      String clientId, String clientSecret, URI tokenUri, int loginAttemptsAllowed) {

    this.tokenUri = tokenUri;

    HttpHeaders headers = new HttpHeaders();
    headers.setBasicAuth(clientId, clientSecret);
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    this.loginEntity = new HttpEntity<>(CLIENT_CREDENTIALS_GRANT, headers);

    if (loginAttemptsAllowed > 0) {
      this.loginAttemptsAllowed = loginAttemptsAllowed;
    } else {
      this.loginAttemptsAllowed = DEFAULT_LOGIN_ATTEMPTS_ALLOWED;
    }
  }

  /**
   * Ensures we have a non-expired oauth token.
   *
   * <p>If we already have an oauth token that isn't expired, this method does nothing. Otherwise,
   * it obtains a new token from the configured {@code tokenUri} before returning. When obtaining a
   * new token, the stored expiration is set to 1 second before the expiration specified by the
   * oauth server. This is done to provide a small buffer before the token expires during which time
   * a new one should be obtained.
   */
  private synchronized void login() {
    if (Instant.now().isBefore(expiration)) {
      return;
    }
    Instant requestTime = Instant.now();
    JsonNode response = doLogin();
    this.token = response.path("access_token").asText();
    int secondsToExpiration = response.path("expires_in").asInt(0);
    this.expiration = requestTime.plusSeconds(secondsToExpiration - 5);
  }

  /**
   * Performs the OAuth2 token exchange. If necessary and allowed by the {@code
   * loginAttemptsAllowed} setting, the token exchange will be retried, up to {@code
   * loginAttemptsAllowed} total attempts. If all attempts have been used unsuccessfully, the
   * exception produced by the latest failed attempt is thrown.
   */
  private JsonNode doLogin() {
    RuntimeException exception = null;
    for (int i = 0; i < loginAttemptsAllowed; i++) {
      try {
        return client.postForObject(tokenUri, loginEntity, JsonNode.class);
      } catch (RuntimeException e) {
        exception = e;
      }
    }
    throw exception;
  }

  /** Performs the request interception to add the authorization header. */
  @Override
  public ClientHttpResponse intercept(
      HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {

    login();

    request.getHeaders().setBearerAuth(token);

    return execution.execute(request, body);
  }
}
